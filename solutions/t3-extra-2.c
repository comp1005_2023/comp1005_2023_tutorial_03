/*
 * https://www.w3resource.com/c-programming-exercises/pointer/c-pointer-exercise-14.php
 */

#include <stdio.h>
int main()
{
    int num[10],i,j,tmp,n;
    int *a = num;
    printf("\n\n Pointer : Sort an array using pointer :\n");
    printf("--------------------------------------------\n");

    printf(" Input the number of elements to store in the array (n <= 10): ");
    scanf("%d",&n);

    if(n < 1 || n > 10) {
        printf("The number of elements to store in the array should be greater than zero, and less than or equal to 100. \n");
        return -1;
    }

    printf(" Input %d number of elements in the array : \n",n);
    for(i=0;i<n;i++)
    {
        printf(" element - %d : ",i+1);
        scanf("%d",a+i);
    }

    for(i=0;i<n;i++)
    {
        for(j=i+1;j<n;j++)
        {
            if( *(a+i) > *(a+j))
            {
                tmp = *(a+i);
                *(a+i) = *(a+j);
                *(a+j) = tmp;
            }
        }
    }

    printf("\n The elements in the array after sorting : \n");
    for(i=0;i<n;i++)
    {
        printf(" element - %d : %d \n",i+1,*(a+i));
    }
    printf("\n");
}
