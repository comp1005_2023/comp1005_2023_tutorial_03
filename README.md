COMP1005 2023 Tutorial 3
========================


## Background

* [Arrays: Part A](https://www.learn-c.org/en/Arrays)
* [Arrays: Part B](https://www.tutorialspoint.com/cprogramming/c_arrays.htm)
    * [Multi-dimensional Arrays](https://www.tutorialspoint.com/cprogramming/c_multi_dimensional_arrays.htm)
    * [Passing arrays to functions](https://www.tutorialspoint.com/cprogramming/c_passing_arrays_to_functions.htm)
    * [Return array from a function](https://www.tutorialspoint.com/cprogramming/c_return_arrays_from_function.htm)
    * [Pointer to an array](https://www.tutorialspoint.com/cprogramming/c_pointer_to_an_array.htm)
* [Pointers: Part A](https://www.learn-c.org/en/Pointers)
* [Pointers: Part B](https://www.tutorialspoint.com/cprogramming/c_pointers.htm)
    * [Pointer arithmetic](https://www.tutorialspoint.com/cprogramming/c_pointer_arithmetic.htm)
    * [Array of pointers](https://www.tutorialspoint.com/cprogramming/c_array_of_pointers.htm)
    * [Pointer to pointer](https://www.tutorialspoint.com/cprogramming/c_pointer_to_pointer.htm)
    * [Passing pointers to functions](https://www.tutorialspoint.com/cprogramming/c_passing_pointers_to_functions.htm)
    * [Return pointer from functions](https://www.tutorialspoint.com/cprogramming/c_return_pointer_from_functions.htm)

## Exercises

0. Write a program in C to store elements in an array and print it.  
    ***Expected Output:*** 
    ```
    Input 10 elements in the array : 
    element - 0 : 9
    element - 1 : 8
    element - 2 : 7
    ...
    element - 9 : 0
    
    Elements in array are: 9 8 7 6 5 4 3 2 1 0
    ```
    
0. Write a program in C to read n number of values in an array and display it in reverse order.  
    ***Expected Output:*** 
    ```
    Input the number of elements to store in the array : 3  
    Input 3 elements in the array : 
    element - 0 : 2
    element - 1 : 5
    element - 2 : 7
    
    The values store into the array are : 
    2 5 7 
    The values store into the array in reverse are : 
    7 5 2 
    ```

0. Write a program in C to find the maximum and minimum element in an array.  
    ***Expected Output:*** 
    ```
    Input the number of elements to store in the array : 3  
    Input 3 elements in the array : 
    element - 0 : 45
    element - 1 : 25
    element - 2 : 21
    
    Maximum element is : 45 
    Minimum element is : 21 
    ```

0. Write a program in C to show the basic declaration of pointer.  
    ***Declaration :***
    ```
    int m=10,n,o;
    int *z=&m ;
    ```

    ***Expected Output:*** 
    ```                                                     
     Here is m=10, n and o are two integer variable and *z is an integer                                          
                                                                                                                  
     z stores the address of m  = 0x7ffd40630d44

     *z stores the value of m = 10
                                                                                
     &m is the address of m = 0x7ffd40630d44
                                                                                                          
     &n stores the address of n = 0x7ffd40630d48
                                                                 
     &o  stores the address of o = 0x7ffd40630d4c
                                                                
     &z stores the address of z = 0x7ffd40630d50
    ```

0. Write a program in C to add two numbers using pointers.  
    ***Expected Output:*** 
    ```
    Input the first number : 5 
    Input the second number : 6 
    
    The sum of the entered numbers is : 11 
    ```

0. Write a program in C to add numbers using function call by reference.  
    ***Expected Output:*** 
    ```
    Input the first number : 5 
    Input the second number : 6 
    
    The sum of the entered numbers is : 11 
    ```

## Extras

0. Write a program in C to print all unique elements in an array.  
    <img src="https://www.w3resource.com/w3r_images/c-array-image-exercise-6.png" width="128">  
    ***Expected Output:*** 
    ```
    Input the number of elements to store in the array : 3  
    Input 3 elements in the array : 
    element - 0 : 1
    element - 1 : 5
    element - 2 : 1
    
    The unique elements found in the array are : 
    5 
    ```
    
0. Write a program in C to print or display upper triangular matrix.  
    <img src="https://www.w3resource.com/w3r_images/c-array-image-exercise-27.png" width="128">  
    ***Expected Output:*** 
    ```
    Input the size of the square matrix : 3 
    Input elements in the square matrix : 
    element - [0],[0] : 1 
    element - [0],[1] : 2 
    element - [0],[2] : 3 
    element - [1],[0] : 4 
    element - [1],[1] : 5 
    element - [1],[2] : 6 
    element - [2],[0] : 7 
    element - [2],[1] : 8 
    element - [2],[2] : 9
    
    The matrix is : 
    1 2 3 
    4 5 6 
    7 8 9 
    
    The upper triangular matrix is:
    1 0 0 
    4 5 0 
    7 8 9 
    ```

0. Write a program in C to sort an array using Pointer.  
    ***Expected Output:*** 
    ```
    Input the number of elements to store in the array : 5  
    Input 5 elements in the array : 
    element - 1 : 25 
    element - 2 : 45 
    element - 3 : 89 
    element - 4 : 15 
    element - 5 : 82 
    
    The elements in the array after sorting :                                                                    
    element - 1 : 15                                                                                             
    element - 2 : 25                                                                                             
    element - 3 : 45                                                                                             
    element - 4 : 82                                                                                             
    element - 5 : 89 
    ```

0. Try other usages of arrays and pointers introduced in the background materials.  
